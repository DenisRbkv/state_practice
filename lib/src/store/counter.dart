import 'package:flutter_super_state/flutter_super_state.dart';
import 'package:flutter/material.dart';


class CounterModule extends StoreModule {
  int get counter => _counter;
  var _counter = 0;

  CounterModule(Store store) : super(store);

  void increment() {
    setState(() {
      _counter++;
    });
  }

  void decrement() {
    setState(() {
      _counter--;
    });
  }

  Color colorSwitcher(Color color) {
    setState(() {});
    return color;
  }

}

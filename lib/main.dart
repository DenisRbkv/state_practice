import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_super_state/flutter_super_state.dart';
import 'dart:math' as math;

import 'src/store/counter.dart';
import 'widgets/drawer_head.dart';
import 'widgets/nav_button.dart';

void main() {
  final store = Store();

  CounterModule(store);

  runApp(StoreProvider(
    store: store,
    child: ExampleApp(),
  ));
}

class ExampleApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: CounterScreen(),
    );
  }
}

class CounterScreen extends StatelessWidget {
  Color color = Color(0xFF10A5F5);
  @override
  Widget build(BuildContext context) {
    return ModuleBuilder<CounterModule>(builder: (context, counterModule) {
      return Scaffold(
        appBar: AppBar(
          title: Text("Counter"),
        ),
        body: Container(
          color: color,
          child: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
            // Our counter module

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                RaisedButton(
                  child: Text("Increment"),
                  onPressed: () => counterModule.increment(),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Text(counterModule.counter.toString(), style: TextStyle(color: Colors.white)),
                ),
                RaisedButton(
                  child: Text("Decrement"),
                  onPressed: () => counterModule.decrement(),
                ),
              ],
            ),
          ]),
        ),
        drawer: Drawer(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              DrawerHead(),
              Padding(
                padding: const EdgeInsets.only(left: 13.0, top: 10.0),
                child: NavButton(() => color = counterModule.colorSwitcher(Color(0xFFE3242B)), 'Red', Color(0xFFE3242B)),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 13.0, top: 5.0),
                child: NavButton(() => color = counterModule.colorSwitcher(Color(0xFF009a00)), 'Green', Color(0xFF009a00)),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 13.0, top: 5.0),
                child: NavButton(() => color = counterModule.colorSwitcher(Color(0xFF10A5F5)), 'Blue', Color(0xFF10A5F5)),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 13.0, top: 5.0),
                child: NavButton(() => color = counterModule.colorSwitcher(Color(0xFFe600e6)), 'Purple', Color(0xFFe600e6)),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 13.0, top: 5.0),
                child: NavButton(() => color = counterModule.colorSwitcher(Color(0xFF000000)), 'Black', Color(0xFF000000)),
              ),
              Divider(),
              Padding(
                padding: const EdgeInsets.only(left: 13.0, top: 5.0),
                child: NavButton(() => color = counterModule.colorSwitcher(Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0)), 'Random', color),
              ),
            ],
          ),
        ),
      );
    });
  }
}
